import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public static urlBase = 'http://127.0.0.1:4000/';
  public static headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  createUser(user): Observable<any> {
    return this.http.post( UserService.urlBase + 'api/user/create' , user);
  }

}
