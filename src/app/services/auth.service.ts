import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';    
    
@Injectable({    
   providedIn: 'root'    
})    
export class AuthService {    

  public static urlBase = 'http://127.0.0.1:4000/';
  public static headers = new HttpHeaders().set('Content-Type', 'application/json');
  userLog: any = {user: 'root', password: 'password'};
  constructor(private http: HttpClient) { }

   login(user: any) : boolean { 
     if( user.user == this.userLog.user && user.password == this.userLog.password)   
        return true; 
      else
        return false;  
    } 

    logged(): boolean {
      if(localStorage.getItem('isLoggedIn') == 'true')
        return true;
      else
        return false;
    }

   logout() :void {    
   localStorage.setItem('isLoggedIn','false');    
   localStorage.removeItem('token');    
   }    
} 