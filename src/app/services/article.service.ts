import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  public static urlBase = 'http://127.0.0.1:4000/';
  public static headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }
  
  createArticle(article): Observable<any> {
    return this.http.post(  ArticleService.urlBase + 'api/articles' , article);
  }

  getArticles(): Observable<any> {
    return this.http.get(  ArticleService.urlBase + 'api/articles');
  }

  getArticle(id: string): Observable<any> {
    return this.http.get(  ArticleService.urlBase + 'api/articles/'+ id);
  }
}
