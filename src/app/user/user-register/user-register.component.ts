import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  checkoutForm;
  valid;
  registered;
  constructor( private formBuilder: FormBuilder, private userService: UserService) {
    this.checkoutForm = formBuilder.group({
      username: '',
      email: '',
      password: '',
      role: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    if(this.checkoutForm.get('username').value.length !== 0 && this.checkoutForm.get('email').value.length !== 0 &&
      this.checkoutForm.get('role').value.length !== 0 && this.checkoutForm.get('password').value.length !== 0) {
      this.valid = true;
      this.userService.createUser({
        username: this.checkoutForm.get('username').value, email: this.checkoutForm.get('email').value,
        password: this.checkoutForm.get('password').value, role: this.checkoutForm.get('role').value
      }).subscribe( param => {
        this.registered = true;
      });
    } else {
      this.valid = false;
    }
  }

}
