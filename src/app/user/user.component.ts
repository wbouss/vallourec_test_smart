import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';  
import { Router } from '@angular/router';
import { ILogin } from '../ILogin';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  model: ILogin = { userid: "admnin", password: "admin@123" }  
  loginForm: FormGroup;  
  message: string;  
  returnUrl: string;  
  submitted: boolean = false;
  logged: boolean = false;

  constructor(  
     private formBuilder: FormBuilder,  
     private router: Router,  
     private authService: AuthService  
  ) {
      this.loginForm = this.formBuilder.group({  
        userid: ['', Validators.required],  
        password: ['', Validators.required]  
    });  
    this.returnUrl = 'article';  
   }  
 
  ngOnInit() {  
    this.logged = this.authService.logged();

  }  
 
  // convenience getter for easy access to form fields  
  get f() { return this.loginForm.controls; }  
 
 
  login() {  
    // stop here if form is invalid  
    if (this.loginForm.invalid) {  
      return;  
    }  
    else {  
      if (this.authService.login({user: this.f.userid.value, password: this.f.password.value})) {
      localStorage.setItem('isLoggedIn', "true");  
      localStorage.setItem('token', this.f.userid.value);  
      this.router.navigate([this.returnUrl]);  
      }  
    else {  
      this.message = "Vérifier vos identifiants";  
      }  
      }  
  }  

  logout() {  
    this.authService.logout();  
    this.router.navigate(['/']);  
    this.ngOnInit();
  } 
 

}
