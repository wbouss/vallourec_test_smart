import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../services/article.service';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  article: any = {name: '' , content: ''};
  articleService: ArticleService;

  constructor(articleService: ArticleService) {
    this.articleService = articleService;
   }

  ngOnInit(): void {
  }

  refresh(element: any): void{
    this.articleService.getArticle(element).subscribe( val => {
      this.article.name = val.name;
      this.article.content = val.content;
    });
  }

}
