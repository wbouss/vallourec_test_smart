import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  checkoutForm;
  articles: Array<any>;
  articlesMatch: Array<any> = new Array();
  @Output() refreshDetail: EventEmitter<number> = new EventEmitter();

  constructor( private formBuilder: FormBuilder, private articleService: ArticleService) {
    this.checkoutForm = formBuilder.group({
      search: '',
      tag: '',
    });
    articleService.getArticles().subscribe( val => {
      this.articles = val['hydra:member'];
      this.articlesMatch = [...this.articles];
      if(this.articles[0])
        this.refreshDetail.emit(this.articles[0].id);

    });
  }

  initArticlesMatch(): void{
    for( let i = 0; i < this.articles.length; i++){
      this.articlesMatch.push(this.articles[i].reference);
    }
    console.log(this.articlesMatch);
  }

  ngOnInit(): void {
  }

  onSubmit(): void{
    if( typeof(this.checkoutForm.get('search').value) == 'undefined' || this.checkoutForm.get('search').value == '')
      this.articlesMatch = [...this.articles];
    else {
      this.articlesMatch = [];
      for( let i = 0; i < this.articles.length; i++){
        if(this.articles[i].reference == this.checkoutForm.get('search').value)
          {
            this.articlesMatch.push(this.articles[i]);
          }
      }
    }
  }

  selectArticle(id: string): void{
    this.refreshDetail.emit(parseInt(id));
  }

}
