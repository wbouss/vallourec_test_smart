import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-article-create',
  templateUrl: './article-create.component.html',
  styleUrls: ['./article-create.component.css']
})
export class ArticleCreateComponent implements OnInit {

  checkoutForm;
  valid;
  created = false;

  constructor( private formBuilder: FormBuilder, private articleService: ArticleService) {
    this.checkoutForm = formBuilder.group({
      name: '',
      reference: '',
      tag: '',
      content: '',
      draft: 'Non'
    });
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    if(this.checkoutForm.get('name').value.length !== 0 && this.checkoutForm.get('reference').value.length !== 0 &&
      this.checkoutForm.get('content').value.length !== 0) {
      this.valid = true;
      this.articleService.createArticle({
        name: this.checkoutForm.get('name').value, 
        reference: this.checkoutForm.get('reference').value,
        content: this.checkoutForm.get('content').value,
        draft: this.checkoutForm.get('draft').value == 'Non' ? false : true,
        user: 1
      }).subscribe( param => {
        this.created = true;
      });
    } else {
      this.valid = false;
    }
  }

}
