import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from './user/user.component';
import {UserRegisterComponent} from './user/user-register/user-register.component';
import {ArticleComponent} from './article/article.component';
import {ArticleListComponent} from './article/article-list/article-list.component';
import { ArticleCreateComponent } from './article/article-create/article-create.component';
import { AuthGuard } from 'src/app/guards/auth.guard';

const routes: Routes = [
  { path: '', component: UserComponent },
  { path: 'user', component: UserComponent },
  { path: 'register', component: UserRegisterComponent },
  { path: 'article', component: ArticleComponent, canActivate : [AuthGuard] },
  { path: 'article/new', component: ArticleCreateComponent, canActivate : [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
