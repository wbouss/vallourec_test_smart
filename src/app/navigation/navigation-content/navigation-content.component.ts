import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navigation-content',
  templateUrl: './navigation-content.component.html',
  styleUrls: ['./navigation-content.component.css']
})
export class NavigationContentComponent implements OnInit {


  constructor(  
    private router: Router,  
    private authService: AuthService  
    ) {
    }
  ngOnInit(): void {
  }

  log(): boolean {
    console.log(localStorage.getItem('isLoggedIn'));
    return this.authService.logged();
  }

  logout() {  
    this.authService.logout();  
    this.router.navigate(['/']);  
  } 

}
